provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "demo-server" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "puppet_security_key"

  vpc_security_group_ids = ["${data.aws_security_group.puppet.id}"]

  provisioner "remote-exec" {
    connection {
    type = "ssh"
    host = self.public_ip
    user = "ubuntu"
    private_key = file("puppet_security_key.pem")
  }

  inline = [
      "sudo puppet agent --test"
    ]
}



  tags = {
    Name = "demo instance"
  }
}

data "aws_security_group" "puppet" {
  name = "puppet"
}


